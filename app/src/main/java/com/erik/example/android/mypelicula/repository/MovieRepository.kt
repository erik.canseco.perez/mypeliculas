package com.erik.example.android.mypelicula.repository

import android.widget.Toast
import com.erik.example.android.mypelicula.data.remoto.MovieApiService

import androidx.lifecycle.MutableLiveData
import com.erik.example.android.mypelicula.app.MyApp
import com.erik.example.android.mypelicula.data.local.MovieRoomDatabase
import com.erik.example.android.mypelicula.data.local.dao.MovieDao
import com.erik.example.android.mypelicula.data.local.entity.MovieEntity
import com.erik.example.android.mypelicula.data.remoto.model.MoviesResponse


import com.erik.example.android.mypelicula.data.remoto.MovieClient
import com.erik.example.android.mypelicula.data.local.entity.Video
import com.erik.example.android.mypelicula.data.remoto.model.VideoResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MovieRepository() {
    private var movieApiService: MovieApiService? = null
    private var movieClient: MovieClient? = null
    private var nowPlaying: MutableLiveData<List<MovieEntity>>? = null
    private var popularMovie: MutableLiveData<List<MovieEntity>>? = null
    private val movieDao: MovieDao

    init {
        movieClient = MovieClient.instance
        movieApiService = movieClient?.getMovieServices()
        nowPlaying = nowPlayingMovies()
        popularMovie = popularMovies()
        movieDao = MovieRoomDatabase.getDataBase().getMovieDao()
    }

    fun nowPlayingMovies(): MutableLiveData<List<MovieEntity>>? {
        if (nowPlaying == null)
            nowPlaying = MutableLiveData<List<MovieEntity>>()

        val call: Call<MoviesResponse>? = movieApiService?.readNowPlaying()
        call?.enqueue(object : Callback<MoviesResponse> {
            override fun onResponse(
                call: Call<MoviesResponse>,
                response: Response<MoviesResponse>
            ) {
                if (response.isSuccessful) {
                    nowPlaying?.value = response.body()?.results
                    Thread {
                        movieDao.saveMovies(response.body()?.results!!)
                    }.start()
                }
            }

            override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
                Toast.makeText(MyApp.instance, "Error de correxion", Toast.LENGTH_LONG).show()
                nowPlaying?.value = movieDao.redNowPlaying()
            }

        })
        return nowPlaying
    }

    fun readVideos(movieId :Int): MutableLiveData<List<Video>>?{
        var video = MutableLiveData<List<Video>>()
        val call : Call<VideoResult>? = movieApiService?.readVideo(movieId)
        call?.enqueue(object : Callback<VideoResult>{
            override fun onResponse(call: Call<VideoResult>, response: Response<VideoResult>) {
               if(response.isSuccessful){
                   video.value = response.body()?.results
               }
            }

            override fun onFailure(call: Call<VideoResult>, t: Throwable) {
                Toast.makeText(MyApp.instance, "Error de correxion", Toast.LENGTH_LONG).show()
            }

        })
        return video
    }

    fun popularMovies(): MutableLiveData<List<MovieEntity>>? {
        if (popularMovie == null)
            popularMovie = MutableLiveData<List<MovieEntity>>()

        val call: Call<MoviesResponse>? = movieApiService?.readPopularMovie()
        call?.enqueue(object : Callback<MoviesResponse> {
            override fun onResponse(
                call: Call<MoviesResponse>,
                response: Response<MoviesResponse>
            ) {
                if (response.isSuccessful) {
                    popularMovie?.value = response.body()?.results

                }
            }

            override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
                Toast.makeText(MyApp.instance, "Error de correxion", Toast.LENGTH_LONG).show()
            }

        })
        return popularMovie
    }
}