package com.erik.example.android.mypelicula.app

import android.app.Application
import android.content.Context

class MyApp : Application() {

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

   companion object {

        lateinit var instance: MyApp
    }
}