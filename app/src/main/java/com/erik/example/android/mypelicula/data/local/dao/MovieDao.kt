package com.erik.example.android.mypelicula.data.local.dao

import androidx.room.Dao
import androidx.lifecycle.LiveData
import androidx.room.Insert
import com.erik.example.android.mypelicula.data.local.entity.MovieEntity
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MovieDao {
    @Query("SELECT * FROM movie")
    fun redNowPlaying(): List<MovieEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveMovies(movieEntityList: List<MovieEntity>)

}