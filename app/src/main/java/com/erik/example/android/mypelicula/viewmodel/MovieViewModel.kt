package com.erik.example.android.mypelicula.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.LiveData
import com.erik.example.android.mypelicula.data.local.entity.MovieEntity
import com.erik.example.android.mypelicula.repository.MovieRepository
import com.erik.example.android.mypelicula.data.local.entity.Video

class MovieViewModel : ViewModel() {
    private val movieRepository: MovieRepository
    private val nowPlayingMovies: LiveData<List<MovieEntity>>

    init {
        movieRepository = MovieRepository()
       nowPlayingMovies = movieRepository.nowPlayingMovies()!!
    }
    fun getNowPlayingMovies():LiveData<List<MovieEntity>>{
        return nowPlayingMovies
    }

    fun getVideoMovie(movieId :Int):LiveData<List<Video>>{
        return movieRepository.readVideos(movieId)!!
    }
}