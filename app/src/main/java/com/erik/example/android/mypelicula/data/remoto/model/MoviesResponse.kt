package com.erik.example.android.mypelicula.data.remoto.model

import com.erik.example.android.mypelicula.data.local.entity.MovieEntity

data class MoviesResponse(
    val page: Int,
    val results: List<MovieEntity>,
    val total_pages: Int,
    val total_results: Int
)