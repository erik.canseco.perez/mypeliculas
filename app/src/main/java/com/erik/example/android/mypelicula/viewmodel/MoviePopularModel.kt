package com.erik.example.android.mypelicula.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.erik.example.android.mypelicula.data.local.entity.MovieEntity
import com.erik.example.android.mypelicula.data.local.entity.Video
import com.erik.example.android.mypelicula.repository.MovieRepository

class MoviePopularModel:ViewModel(){

    private val movieRepository: MovieRepository
    private val popularMovies: LiveData<List<MovieEntity>>

    init {
        movieRepository = MovieRepository()
       popularMovies = movieRepository.popularMovies()!!
    }
    fun getPopularMovies(): LiveData<List<MovieEntity>> {
        return popularMovies
    }

}