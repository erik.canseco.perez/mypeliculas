package com.erik.example.android.mypelicula.data.remoto.model

import com.erik.example.android.mypelicula.data.local.entity.Video

data class VideoResult(
    val id: Int,
    val results: List<Video>
)