package com.erik.example.android.mypelicula.data.local

import androidx.room.Database
import androidx.room.Room
import com.erik.example.android.mypelicula.data.local.entity.MovieEntity
import androidx.room.RoomDatabase
import com.erik.example.android.mypelicula.app.MyApp
import com.erik.example.android.mypelicula.data.local.dao.MovieDao


@Database(entities = [MovieEntity::class], version = 1)
abstract class MovieRoomDatabase : RoomDatabase() {

    abstract fun getMovieDao(): MovieDao

    companion object{
        private var INSTANCE:MovieRoomDatabase? = null

        fun getDataBase():MovieRoomDatabase{
            return INSTANCE ?: synchronized(this){
                val instance = Room.databaseBuilder(
                    MyApp.instance.applicationContext,
                    MovieRoomDatabase::class.java,
                    "movies"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                instance
            }
        }
    }
}